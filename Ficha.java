//Clase Ficha, proyecto programado 
//Integrantres: Kiara, Jimena, Angel, Paulina 

import javax.swing.JPanel;
import javax.swing.ImageIcon;

public class Ficha extends JPanel
{
	private String color; 
	private String tipo;
	ImageIcon ficha;
	
	public Ficha (String color, String tipo)
	{	
		setColor(color);
		setTipo(tipo);
		ficha=new ImageIcon("pieza.jpg");
		
	}//fin metodo constructor con parametros
	
	public void setColor (String color)
	{
		this.color=color;
	}//fin setcolor
	
	public String getColor ()
	{
		return color;
	}//fin getcolor
	
	public void setTipo (String tipo) 
	{
		this.tipo=tipo;
	}//fin setTipo
	
	public String getTipo()
	{
		return tipo;
	}//fin getTipo
	
	public String toString ()
	{
		return "La ficha que se encuentra es: "+getTipo()+" El color de la ficha es: "+getColor();
	}//fin toString
	
	//public void colorAleatorioFicha ()
	//{
		//String color1="negro";
		//String color2="blanco";
		
		//colorFicha=(String)(Math.random()*2+1);
	  
	//}//fin colorAleatorio
	
}//fin de la clase 
